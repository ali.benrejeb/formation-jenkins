package fr.formation.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import fr.formation.buisness.beans.Formation;
import fr.formation.commons.ObjetsCommuns;
import fr.formation.dao.DaoImpl;
import fr.formation.services.IServiceFormation;
import fr.formation.services.ServiceFormateur;
import fr.formation.vo.PersonneVo;

public class FormateurServlet extends HttpServlet {
	
	private IServiceFormation service;
	
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		
		 HttpSession session = req.getSession();
	     PersonneVo personne = (PersonneVo) session.getAttribute(ObjetsCommuns.SessionBeans.SESSION_PERSONNE);
		
	     service = new ServiceFormateur(DaoImpl.getInstance());
//		 List<Formation> formations = service.getListFormationParIdPersonne(personne.getId());
		 List<Formation> formations = service.getListFormationParIdFormateur(personne.getId());
		 req.setAttribute(ObjetsCommuns.Param.CHAMP_FORMATIONS_FORMATEUR , formations);
		 //il faut penser a rajouter un filtre ds le web.xml sinon
		 //un non formateur aura acces a cette page
		// resp.sendRedirect( req.getContextPath() + ObjetsCommuns.Url.VUE_ESPACE_PRIVE_ADMIN  );
	 
		
		this.getServletContext().getRequestDispatcher( ObjetsCommuns.Url.VUE_ESPACE_PRIVE_ADMIN ).forward( req, resp );
	
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		 	
	    
	}
	

	
	
}
