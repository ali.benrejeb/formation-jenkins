package fr.formation.buisness.beans;

public class PersonneDao {

	private String nom;
	private String prenom;
	private int age;
	private String email;
	private String mdp;
	private int id;
	private boolean isFormateur;
	
	
	
	
	
	public boolean isFormateur() {
		return isFormateur;
	}
	public void setFormateur(boolean isFormateur) {
		this.isFormateur = isFormateur;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getMdp() {
		return mdp;
	}
	public void setMdp(String mdp) {
		this.mdp = mdp;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getPrenom() {
		return prenom;
	}
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	@Override
	public String toString() {
		return "Personne [nom=" + nom + ", prenom=" + prenom + ", age=" + age
				+ ", email=" + email + ", mdp=" + mdp + "]";
	}
	
	
	
	
}
