package fr.formation.buisness.traitement;

import fr.formation.buisness.beans.PersonneDao;
import fr.formation.vo.PersonneVo;

public class FactoryPersonne {
	
	
	public static PersonneDao builPersonne(PersonneVo personneVo){
		PersonneDao p = new PersonneDao();
		p.setNom(personneVo.getNom());
		p.setPrenom(personneVo.getPrenom());
		p.setEmail(personneVo.getEmail());
		p.setMdp(personneVo.getMdp());
		p.setFormateur(personneVo.isFormateur());
		
		
		return p;
	}
	
	
	public static PersonneVo builPersonneVo(PersonneDao personne){
		PersonneVo p = new PersonneVo();
		p.setNom(personne.getNom());
		p.setPrenom(personne.getPrenom());
		p.setEmail(personne.getEmail());
		p.setMdp(personne.getMdp());
		p.setId(personne.getId());
		p.setFormateur(personne.isFormateur());
		return p;
	}


}
